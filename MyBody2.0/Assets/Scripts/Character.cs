﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{

	public int Health;
	public float VelAttack;
	public float VelMovement;
	public GameObject Bullet;
	public float[] BulletOffsets;
	public List<AudioClip> Sounds;
	public List<string> NameSounds;
	public int DirectAttack;
	private int Type; // 1-Player, 2-Enemy, 3-Ally, 4-Boss. 
	private bool flipped = false;
	private GameObject Target;
	private AudioSource AudioSrc;
	private Animator Anim;


    // Start is called before the first frame update
    void Start()
    {
		AudioSrc = GetComponent<AudioSource>();
		Anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
		Attack();
    }

	void Attack()
	{
		if(Type == 1){
			if(Input.GetMouseButtonDown(0)){
				GameObject newBullet = Instantiate(Bullet, transform.position + new Vector3(BulletOffsets[0] * ( (flipped) ? -1 : 1 ), BulletOffsets[1], 0), transform.rotation);
				newBullet.GetComponent<Projectile>().SetKillType(new int[]{2,4});
				newBullet.GetComponent<Projectile>().SetTarget(Input.mousePosition);
				Anim.SetBool("isShooting", true);
			}else if(Input.GetMouseButtonDown(1)){
				GameObject newBullet = Instantiate(Bullet, transform.position + new Vector3(BulletOffsets[0] * ( (flipped) ? -1 : 1 ), BulletOffsets[1], 0), transform.rotation);
				newBullet.GetComponent<Projectile>().SetKillType(new int[]{2,4});
				newBullet.GetComponent<Projectile>().SetTarget(Input.mousePosition);
				Anim.SetBool("isBurning", true);
			}else {
				Anim.SetBool("isShooting", false);
				Anim.SetBool("isBurning", false);
			}
		}else if(Type == 2){
			
		}else if(Type == 3){
			
		}else if(Type == 4){
			
		}

	}

	void Move()
	{
		Vector3 newPosition = Vector3.zero;
		Vector3 mousePosition = Vector3.zero;
		if(Type == 1){
			newPosition = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
			mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
			flipped = (mousePosition.x < 0);
			Anim.SetBool("isMoving", newPosition != Vector3.zero);
		}else if(Type == 2){
			if(Target){
				if(Vector3.Distance(transform.position, Target.transform.position) < 10 ){
					newPosition = Target.transform.position - transform.position;
					newPosition.z = 0;
				}
				flipped = (newPosition.x > 0);
			}
		}
		newPosition.Normalize();
		newPosition *= VelMovement;
		transform.Translate(newPosition * Time.deltaTime);
		GetComponent<SpriteRenderer>().flipX = flipped;
	}

	public void Damage(int value)
	{
		Health -= value;
		if(Health <= 0){
			Destroy(gameObject);
		}
	}

	public void SetTarget(GameObject Player){
		Target = Player;
	}

	public void SetType(int nType){
		Type = nType;
	}

	public int GetChType(){
		return Type;
	}

	public void PlaySound(string Name){
		int index = NameSounds.IndexOf(Name);
		AudioSrc.clip = Sounds[index];
		//AudioSrc.volume = Volume;
		AudioSrc.Play();
	}

	public void StopSound(){
		AudioSrc.Stop();
	}

	void OnCollisionEnter2D(Collision2D coll)
    {   
        GameObject obj = coll.gameObject;
		if(Type == 2 || Type == 4){
			if(obj.GetComponent<Character>()){
				if( obj.GetComponent<Character>().GetChType() == 1  || obj.GetComponent<Character>().GetChType() == 2 ){
					obj.GetComponent<Character>().Damage(DirectAttack);
				}
        	}
			Anim.SetBool("isAttacking", true);
		}
    }

	void OnCollisionExit2D(Collision2D coll){
		if(Type == 2 || Type == 4){
			Anim.SetBool("isAttacking", false);
		}
	}
	
}
