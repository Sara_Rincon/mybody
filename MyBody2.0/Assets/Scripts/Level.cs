﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public Vector3[] ControllPoints;
    public int[] Size = new int[2];
    public GameObject[] Enemies;
    public GameObject Boss;
    public GameObject Character;
    public int NumEnemies;
    public Vector3 StartPoint, EndPoint;
    private List<GameObject> EnemiesCreated = new List<GameObject>();


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadEnemies(){
        System.Random Rand = new System.Random();
        for (int i = 0; i < NumEnemies; i++)
        {
            GameObject Enemy = Enemies[Rand.Next(0, Enemies.Length)];
            Vector3 position = new Vector3(Rand.Next(0, Size[0]) - (int) (Size[0]/2), Rand.Next(0, Size[1]) - (int) (Size[1]/2), 0);
            EnemiesCreated.Add(Instantiate(Enemy, position, Quaternion.identity));
            EnemiesCreated[EnemiesCreated.Count-1].GetComponent<Character>().SetType(2);
        }
        
    }

    public void SetPlayer(GameObject Player){
        
        foreach (GameObject enemy in EnemiesCreated)
        {
            enemy.GetComponent<Character>().SetTarget(Player);
        }
    }
}
