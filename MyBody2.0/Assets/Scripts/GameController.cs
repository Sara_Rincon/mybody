﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject Player;
    public GameObject[] Levels;
    public GameObject Camera;
    private int currentLevel = 0;

    void Start()
    {   
        GameObject Level = Instantiate(Levels[currentLevel], Vector3.zero, Quaternion.identity);
        Player.GetComponent<Player>().Init(Level.GetComponent<Level>().Character, Level.GetComponent<Level>().StartPoint);
        
        Level.GetComponent<Level>().LoadEnemies();
        Level.GetComponent<Level>().SetPlayer(Player.GetComponent<Player>().GetCharacter());
    
        Camera = Instantiate(Camera, Level.GetComponent<Level>().StartPoint + new Vector3(0,0,-10), Quaternion.identity);
        Camera.GetComponent<CameraController>().Init(Player.GetComponent<Player>().GetCharacter());

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
