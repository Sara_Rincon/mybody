﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector3 offset = Vector3.zero;
    private GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LateUpdate () 
    {
        if(Player){
            transform.position = Vector3.Lerp(transform.position, Player.transform.position+offset, 0.1f);
        }
        
    }

    public void Init(GameObject nPlayer){
        Player = nPlayer;
        offset = transform.position - Player.transform.position;
    }
}
