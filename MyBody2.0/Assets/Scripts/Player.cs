﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private GameObject Character;
    public GameObject PowerUp;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Init(GameObject nCharacter, Vector3 position){
        Character = Instantiate(nCharacter, position, Quaternion.identity);
        Character.GetComponent<Character>().SetType(1);
    }

    public GameObject GetCharacter(){
        return Character;
    }
}
