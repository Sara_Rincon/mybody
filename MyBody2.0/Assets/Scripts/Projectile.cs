﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
	public int Damage;
	public float Velocity;

	private Vector3 Target;
    private int[] KillType;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    public void SetTarget(Vector3 nTarget)
    {
        nTarget.z = 0;
        Target = Camera.main.ScreenToWorldPoint(nTarget) - transform.position;
        Target.z = 0; 
        Target.Normalize();
        Target *= Velocity;
    }

	public void Shoot()
	{
		transform.Translate(Target * Time.deltaTime);
	}

    void OnCollisionEnter2D(Collision2D coll)
    {   
        Debug.Log("Collide");
        GameObject obj = coll.gameObject;
        if(obj.GetComponent<Character>()){
            if( System.Array.IndexOf( KillType, (obj.GetComponent<Character>().GetChType()) ) >= 0 ){
                obj.GetComponent<Character>().Damage(Damage);
                Target = Vector3.zero;
                Destroy(gameObject, 5);
            }
        }else if(!obj.GetComponent<Projectile>()){
            Target = Vector3.zero;
            Destroy(gameObject, 5);
        }
    }

    public void SetKillType(int[] ktype){
        KillType = ktype;
    }

}
